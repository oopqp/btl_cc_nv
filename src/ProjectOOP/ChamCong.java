/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectOOP;

/**
 *
 * @author ddq
 */
public class ChamCong {
    String maNV;
    Integer maCC;
    String ngayCham;
    Float soGioLam;
    
    public ChamCong(){}

    public String getmaNV() {
        return maNV;
    }

    public void setmaNV(String maNV) {
        this.maNV = maNV;
    }

    public Integer getMaCC() {
        return maCC;
    }

    public void setMaCC(Integer maCC) {
        this.maCC = maCC;
    }

    public String getNgayCham() {
        return ngayCham;
    }

    public void setNgayCham(String ngayCham) {
        this.ngayCham = ngayCham;
    }

    public Float getSoGioLam() {
        return soGioLam;
    }

    public void setSoGioLam(Float soGioLam) {
        this.soGioLam = soGioLam;
    }
    
}
