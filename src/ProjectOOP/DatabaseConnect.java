/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectOOP;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phuong
 */
public class DatabaseConnect {

private final String className = "com.mysql.jdbc.Driver";
    private final String url = "jdbc:mysql://localhost:3306/ChamCong";
    private final String user ="root";
    private final String pass = "admin@123";
    private final String table_nv = "nhan_vien";
    private final String table_cc = "cham_cong";
    private Connection connection;
    
    public void connect(){
        try {
            Class.forName(className);
            try {
                connection = DriverManager.getConnection(url,user,pass);
                System.out.println("Kết nối thành công!\n");
            } catch (SQLException ex) {
                System.out.println("Kết nối thất bại.\n");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Kết nối thất bại.\n");
        }
        
    }
    
  
    public ResultSet getDataNV(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    
    public ResultSet getDataCC(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select cham_cong.MaNV ,TenNV, NgayCham, SoGioLam, MaCham"
                + " from " + table_cc + " , " + table_nv
                + " where nhan_vien.MaNV = cham_cong.MaNV order by cham_cong.MaNV, NgayCham ASC";
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    
    public ResultSet getDataCCGui(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select MaNV, TenNV "
                + " from " + table_nv ;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    
    public ResultSet getNgayThangCC(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select max(NgayCham) "
                + " from " + table_cc ;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getMaCCMax(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select max(MaCham) "
                + " from " + table_cc ;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getNgayVaoMaxNV(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select max(NgayVaoLam) "
                + " from " + table_nv ;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getMaNVMax(){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select max(MaNV) "
                + " from " + table_nv ;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getLuongByThangNam(String ma){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select month(NgayCham), year(NgayCham), "
                +" LuongCoBan*sum(SoGioLam)/(8*30) from "
                + table_cc + " , " + table_nv
                +" where nhan_vien.MaNV = cham_cong.MaNV and cham_cong.MaNV = '" + ma                
                +"' group by month(NgayCham), year(NgayCham)";
        
        try {
           
            st = connection.createStatement();
            
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select lay luong!\n"+ ex.toString());
        } 
        return rs;
    }
    
    
    public ResultSet getCanhCaoByThangNam(String ma){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select month(NgayCham), year(NgayCham), count(SoGioLam) from "
                + table_cc + " , " + table_nv
                +" where nhan_vien.MaNV = cham_cong.MaNV and SoGioLam = 0 and cham_cong.MaNV = '" + ma                
                +"' group by year(NgayCham), month(NgayCham)";
        
        try {
           
            st = connection.createStatement();
            
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select canh cao!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataCCByNgayCham(String ngayCham){
        Statement st;
        ResultSet rs = null;
        String sqlCommand = "select cham_cong.MaNV ,TenNV, NgayCham, SoGioLam"
                + " from " + table_cc + " , " + table_nv
                + " where nhan_vien.MaNV = cham_cong.MaNV and NgayCham like '" + ngayCham + "'"
                + " order by cham_cong.MaNV, NgayCham ASC" ;
        try {
            
            st = connection.createStatement();
            rs = st.executeQuery(sqlCommand);
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataMaSo(String maNV){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where MaNV = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, maNV);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataHoTen(String ten){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where TenNV = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, ten);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataNgaySinh(String ns){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where NgaySinh = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, ns);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataDiaChi(String dc){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where DiaChi = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, dc);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataSDT(String ns){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where SDT = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, ns);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataVaoLam(String ns){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where NgayVaoLam = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, ns);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
    
    public ResultSet getDataLuongCB(String ns){
        ResultSet rs = null;
        String sqlCommand = "select * from " + table_nv + " where LuongCoBan = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, ns);
            rs = pst.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Lỗi câu lệnh select!\n"+ ex.toString());
        } 
        return rs;
    }
     
    
    public void DeletemaNV(String maNV){
        String sqlCommand = "delete from " + table_nv + " where MaNV = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            
            pst.setString(1,maNV);
            if(pst.executeUpdate()>0)
                System.out.println("Xóa thành công!\n");
            else 
                System.out.println("Xóa không thành công!\n");
        } catch (SQLException ex) {
            System.out.println("Xóa không thành công!\n"+ ex.toString());
        }        
    }
    
    public void Insert(NhanVien s){
        String sqlCommand = "insert into " + table_nv + " value(?,?,?,?,?,?,?)";
        PreparedStatement pst =null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(1, s.getMaNV());
            pst.setString(2, s.getTenNV());
            pst.setString(3, s.getDiaChi());
            pst.setString(4, s.getSDT());
            pst.setInt(5, s.getLuongCoBan());
            pst.setString(6, s.getNgaySinh());
            pst.setString(7, s.getNgayVaoLam());
            if(pst.executeUpdate()>0)
                System.out.println("Thêm thành công!\n");
            else 
                System.out.println("Thêm thất bại!\n");
        } catch (SQLException ex) {
            System.out.println("Thêm thất bại!\n"+ ex.toString());
        }
    }
    
    
    public void InsertCC(ChamCong cc){
        String sqlCommand = "insert into " + table_cc + " value(?,?,?,?)";
        PreparedStatement pst =null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            pst.setString(2, cc.getmaNV());
            pst.setInt(1, cc.getMaCC());
            pst.setString(3, cc.getNgayCham());
            pst.setFloat(4, cc.getSoGioLam());
      
            if(pst.executeUpdate()>0)
                System.out.println("Thêm thành công!\n");
            else 
                System.out.println("Thêm thất bại!\n");
        } catch (SQLException ex) {
            System.out.println("Thêm thất bại!\n"+ ex.toString());
        }
    }
    
    public void UpdateId(String maNV, NhanVien s){
        String sqlCommand = "update " + table_nv + " set TenNV = ?, DiaChi = ?, SDT = ?, "
                + " LuongCoBan = ?, NgaySinh = ?, NgayVaoLam = ? where MaNV = ?";
        PreparedStatement pst = null;
        try {
            
            pst = connection.prepareStatement(sqlCommand);
            
            pst.setString(1, s.getTenNV());
            pst.setString(2, s.getDiaChi());
            pst.setString(3, s.getSDT());
            pst.setInt(4, s.getLuongCoBan());
            pst.setString(5, s.getNgaySinh());
            pst.setString(6, s.getNgayVaoLam());
            pst.setString(7, s.getMaNV());
            
            if(pst.executeUpdate()>0)
                System.out.println("Sửa thành công!\n");
            else 
                System.out.println("Sửa thất bại!\n");
        } catch (SQLException ex) {
            System.out.println("Sửa thất bại!\n"+ ex.toString());
        }
    }
    
    
    
    public static void main(String[] args) {
        DatabaseConnect myconnect = new DatabaseConnect();
        myconnect.connect();
        //myconnect.showData(myconnect.getData());
    }
    
}
